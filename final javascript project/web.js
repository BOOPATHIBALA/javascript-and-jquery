let list = "abcdefghijklmnopqrstuvwxyz1234567890!@#$%^&*()_+-=";
let password = "";

function randompass(length){
    password = "";
    
    for(i=0;i<length;i++){
        password+=list.charAt(Math.floor(Math.random() * list.length));   // charAt() => is printed by starting value.
    }
    return password;
}

function passgen(length){
    document.output.pass.value = randompass(length);
}