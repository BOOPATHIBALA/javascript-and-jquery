// 1. it is a line through:


// $('li').on('click',function(){
//     if(this.style.color != 'grey'){
//         $(this).css('color','grey');
//         $(this).css('text-decoration','line-through');
//     }
//     else{
//         $(this).css('color','black');
//         $(this).css('text-decoration','none');
//     }
// })



            // (or)

// this step using means:
//     create a new class in css File.thus class_name is used to the toggleClass().

$('ul').on('click','li',function(){
    $(this).toggleClass('grey');
})




// 2. delete the list:

$('ul').on('click','span',function(event){
    $(this).parent().fadeOut(1000,function(){      // parent() => it is used to find the parent node.
        $(this).remove()                           // this line 'this' keyword is includes the spam and li tag.
    });
    event.stopPropagation()                        // stopPropagation => is used to only click the current tag. otherwise stop the click.
})






// Add a todo list:

$('input').on('keypress',function(event){
    if(event.which === 13){                     // 13 => is a 'enter' code. which => is used to show the event number.
        let val = $(this).val() //(or) event.target.value;
        $(this).val('')                     // val('') => it is used to empty the values.            

        $('ul').append(`<li><span><i class="bi bi-x-circle-fill"></i></span> ${val}</li>`)
    }
})

// the new list tag should be added means, thus the tag doen't followed by the line-through and remove.
// then, alter the both upper program.