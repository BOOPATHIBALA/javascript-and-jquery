
// changing the text contents:

// document.querySelector('h1').textContent = 'APJ';    // it is used a javascript format.

$('h1').text('APJ');     // it is a jquery format.





// if changing the 'ul' 'li' tag first & last contents change means:

$('li:first').text('first');            // change the first list tag content
$('li:last').text('last');             // change the last list tag content






// if using 2 'a'- tag means, thus 'a' tag contents shoulld be change:

$('a:first').text('Apj first')
$('a:last').text('Apj last')

        //  (or) 
$('ul li a').text('Apj alter first')





// css using jQuery

$('h1').css('background','red');





// object using jQuery

let object = {
    'background': 'grey',
    'color': 'white',
    'border' : '5px solid blue'
}
$('ul').css(object);