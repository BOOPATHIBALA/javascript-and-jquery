
// attr => is used to add the attributes. e.x: width, height, src.

$('img').attr('src','https://images.pexels.com/photos/1779487/pexels-photo-1779487.jpeg?auto=compress&cs=tinysrgb&w=600')

$('img').attr('width','700px')



// val() => is used to add the substitute values. it is only using for input tags.
$('input').val('javascript')
// if values are reset means:
$('input').val('');



// add the class means => addClass().
// $('h1').addClass('idh')
// $('.idh').css('color','red');


// remove the class means => removeClass().
$('h1').removeClass('idh')