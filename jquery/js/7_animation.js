

//  toggle is => out means in (or) in means out.

$('.mybtn').on('click',function(){
    $('.cls1').slideToggle(1000,function(){       // if suppose 'print any contents' and 'remove any tags', after animation means, using call back function.
        console.log("clicked");
            // or
        // $(this).remove();                    // if animation is once print and remove means, this statement to be used.
    })
})