
// Arrow function is a single statement function.in this function using means, doesn't used function keyword.
// it is using means , return and '{}' are not used. in this method is not using 'this' keyboard.



// syntax:
// () => {
// }



let name1 = (name) => `hello ${name}`;

console.log(name1('ashwin'));



let game=[{
    name2: 'basketball',
    players: 12,
    aim: 'basket',
    isdone: true
},
{
    name2: 'volleyball',
    players: 12,
    aim: 'cap',
    isdone: true
}];



// filter() => is a filtering the single object

let filter=game.filter(function(todo){
    return todo.isdone==true
})

console.log(filter);



let select = game.filter((todo) => todo.isdone===true)
console.log(select);