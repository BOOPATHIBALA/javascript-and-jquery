
// if error in the program means, thus the next statement shouldn't executed.thus error should be rectify means => try catch() used.



let result = function(num){
    if(typeof num === 'number'){
        return num*10;
    }
    else{
        throw Error("it is not a number");                // this statement is showing the error message. it is similar to conole.log.
    }
}

try {
    console.log(result('10'));
} catch (error) {
    console.error(error);                                // this statement is used to show the errors in error format.
    console.log(error);                                  // this statement is used to show the errors in text format.
}

console.log("this a multiplication function");