

// DOM(document object model) => start. 

// document => whole html document. (ex):<!DOCTYPE html>

// object   => head and body is an object.

// model    => means, which is parent tag to be finds.


console.log(document.body);           // it's used to show the head tag (or) body tag (or) title .




// if replace the 'title' value means:
document.title = 'frankeey';





// if show the body tag contents means:

console.log(document.getElementById('idfst'));    // document.getElementById => it is used to show the id based contents.
console.log(document.getElementsByClassName('classfst'));    // document.getElementByClassName => it is used to show the class based contents.





// if we used 4 p_tag means, print all the p-tag:
// querySelectorAll => is print the array format only. 

console.log(document.querySelector('p'));         // it is only used to print the initial tag.

console.log(document.querySelectorAll('p'));      // it is used to print the all the p-tag in array format.

// thus array is used to print the separate tag means:

let list = document.querySelectorAll('p');
console.log(list[2]);





// document.querySelectorAll => is used to print id and class means:

let id = (document.querySelectorAll('#idfst'));
let class1 = (document.querySelectorAll('.classfst'));

console.log(id[0]);
console.log(class1[0]);

