// addEventListener => is used to perform the tags. 

// event => is a array actions. 

document.querySelector('button').addEventListener('click',(event)=>{
    
    event.target.textContent='Clicked';                       // this is only using for button tag.
    
            // or
    
    document.querySelector('h1').textContent='JavaScript';    // this is using for any content tag. e.x: button,h1 or p.
    
})