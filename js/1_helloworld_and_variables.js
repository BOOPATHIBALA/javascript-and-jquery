    // helloworld

console.log("Hello World");


    // variable

// string:
let name="ashwin";
console.log(name);


// integer:
let number1=10;
let number2=10;
number1=20;
console.log(number1+number2);

// if suppose integer using number means:
let num1='10';
let num2=2;
console.log(num1+num2);              // output => 102  10=>string and 2=>number.


    // constant value => it's doesn't change the value.
const number=10;
number=11;
console.log(number);        // error




