let game=["cricket","basket ball","volley ball"];

// separate array value means: 
console.log(game[0]);
console.log(game[1]);
console.log(game[2]);

// normal array length:
console.log(game.length);  



// two ways to print the whole array
    
console.log(game);         // its a array format
console.log(`${game}`);    // its a normal format



// two  ways finding array length with contens  means:

console.log("we have "+game.length+" games");
console.log(`we have ${game.length} games`);



// if suppose add extra array values :


let allow =['hello','world','name'];

console.log(`${allow}`);

allow.push(' push => boo');      //add new array value in last.

console.log(`${allow}`);

allow.unshift('unshift => boo ');      //add new array value in first.
console.log(`${allow}`);