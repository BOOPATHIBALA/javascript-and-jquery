// normal function:

function hello(){
    console.log("Hello");
}
hello();


// parameterized function:

function name1(fname,age){
    console.log(fname,age);
}
name1('ashwin',21);


// mathematic action:

function add(num1,num2){
    return num1+num2;
}
console.log(add(1000,2000));


// variable using => function:

let list = function(name){
    console.log(name);
}
list('ashwin');