// 'var' and 'let' keyword are also similar.its only differ from scope.'var' is old version and 'let' is new version.

let name1='ashwin';
var name2="ashwin";

console.log(name1);
console.log(name2);



// differ from let and var:

if(true){
    let name3='let';
    var name4='var';
}
console.log(name4);      // its working => its a 'var' keyword.
// console.log(name3);   // error: let name3 => it is a local variable. then its not working for outer 'if' statement.



// if suppose one variable using multiple scope means => 'let' keyword.

if(true){
    let name5='let';
    var name6='var';
    console.log(name5);
    console.log(name6);  
}
if(true){
    let name5='yello';                  // 'let' keyword => is using to redeclare for same variable. but,var is not redeclare for same variable.
    console.log(name5);                 
}
