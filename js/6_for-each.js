    //  foreach loop

let games=["cricket","basket ball","volley ball"];

// syntax:
// array.forEach(element => {
// });


games.forEach(function(game,index){
    console.log(`${index+1}. ${game}`);
});

