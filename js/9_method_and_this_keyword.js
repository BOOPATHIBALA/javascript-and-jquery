// what is method => ex: console.log => in this example using '.' . this '.' is using means, that is a method.


let website={
    title: "hacking",
    author: "ashwin",
    price: "1999",


    printfunction:function(name){
        console.log(name);
    },

    changeprize:function(newprice){
        // console.log(this);         => this statement is used to view the object.
        this.price=newprice;
    }
}

website.printfunction('aswin');

website.changeprize('99');

console.log(`title is ${website.title} => author is ${website.author} => price is ${website.price}`)